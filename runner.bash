#!/bin/bash
agents=( 3 4 8 12)
for Nagents in "${agents[@]}"
do
    echo "--------------------------------------------"
    echo "#agents: $Nagents"
    echo "--------------------------------------------"
    sbt "run $Nagents"
done