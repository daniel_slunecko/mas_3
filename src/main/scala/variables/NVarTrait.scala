package variables

trait NVarTrait {
  lazy val fullDomain:Set[NVal] = domain(n) map (e => NVal(n,e))
  val n:Int
  var curValue:NVal
}
