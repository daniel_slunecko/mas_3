package variables

/**
  * this class represents a value with domain 0 to N
 */
case class NVal(n:Int,value:Int) extends Ordered[NVal]{
  override def compare(that: NVal):Int = this.value compare that.value
  def toInt:Int = value
  def isValid:Boolean = value >= 0 && value < n
}
/**
  * this class represents a value with domain 0 to N and a timestamp
  */
case class NValWithTime(n:Int,value:Int, time:Long){
  def toNVal = NVal(n,value)
  def toInt:Int = value
  def isValid:Boolean = value >= 0 && value < n
}
case object NValWithTime{
  def apply(n1:Int,value1:NVal,time1:Time):NValWithTime = NValWithTime(n1,value1.toInt,time1.time)
}
