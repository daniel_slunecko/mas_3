package variables

/**
  * variable with possible domain, usd for representation of current position for the agents
  * @param n      size of the domain
  * @param id     id of the agent
  */
case class NVar(n:Int,id:Int) extends NVarTrait {
  override var curValue:NVal = NVal(n,0)
  var curDomain:Set[NVal] = fullDomain

  def apply(value:Int):Unit = curValue = NVal(n,value)
  def apply(value:NVal):Unit = curValue = value
  def removeFromDomain(values:Int*):Unit = curDomain = curDomain.diff(values.map(NVal(n,_)).toSet)
  def negate:Set[NVal] = negation(curValue) intersect curDomain
}