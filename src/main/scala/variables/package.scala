package object variables {
  def domain(n:Int):Set[Int] = (0 until n).toSet

  def negation[A <: NVal](nvl:A):Set[NVal] = setNegation(nvl)

  def setNegation[A <: NVal](as:A*):Set[NVal] = {
    val l = as.toSet
    val n = l.head.n
    domain(n).diff(l.map(_.value)).map(value => NVal(n,value))
  }

  def initVals(n:Int):Array[NVal] = domain(n).map(v => NVal(n,v)).toArray
}
