package variables

/**
  * a time stamp. used for counting number of moves executed by a specific agent
  * @param time
  */
case class Time(time:Long){
  def nextTime:Time = Time(time+1)
}
case object Time{
  lazy val Beginning = Time(0)
}
