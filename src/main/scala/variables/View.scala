package variables

import scala.collection.mutable.{Map => MMap}
import View._

import NoGood.EmptyNGood

case class View(n:Int,id:Int) {
  private val view:MMap[Int,NValWithTime] = MMap()      // here we store the information about other queens
                                                        // here we store functions for computation of blocked positions
  private val blocks:Map[Int,NVal=>Set[NVal]] = (0 until n).map(key => key -> getBlocked(id)(key) _ ).toMap
  private val position:NVar = NVar(n,id)                // here we store current position of given queen

  // current position
  def curPositon:NVal = position.curValue
  // current domain
  def curDomain:Set[NVal] = position.curDomain
  // accessor for views
  def apply(key: Int):NValWithTime = view(key)
  // contains key
  def containsKey(key:Int):Boolean = view contains key
  // update positions of others
  def put(key:Int,value:NValWithTime):Unit = view put(key,value)
  // remove knowledge about other position of neighbour
  def remove(key:Int):Unit = view remove key
  // recompute current domain
  def recomputeDomain(ngdb:NGDB):Unit = position.curDomain = curDomain -- ngdb(EmptyNGood)
  // is x_i inconsistent with the view?
  def isInconsistent(ngdb:NGDB):Boolean = !isConsistent(ngdb:NGDB)
  // is x_i consistent with the view?
  def isConsistent(ngdb:NGDB):Boolean = getNonBlocked(ngdb:NGDB) contains curPositon
  // is there some consistent value?
  def existsConsistent(ngdb:NGDB):Boolean = getNonBlocked(ngdb).nonEmpty
  // can this agent move?
  def canMove(ngdb:NGDB):Boolean = (getNonBlocked(ngdb:NGDB) - curPositon).nonEmpty
  // move this agent to position
  def move2(newPosition:NVal):Unit = position(newPosition)
  // non blocked positions
  def getNonBlocked(ngdb:NGDB):Set[NVal] = curDomain diff allBlocked(ngdb:NGDB)
  // get all blocked positions
  def allBlocked(ngdb:NGDB):Set[NVal] = blockedPure ++ blockedByNoGoods(ngdb:NGDB)
  // get positions blocked purely by positions of others
  private def blockedPure:Set[NVal] = (view.keys filter (_ < id) flatMap (key => blocks(key)(view(key).toNVal))).toSet
  // get positions blocked by active NoGoods
  private def blockedByNoGoods(ngdb:NGDB):Set[NVal] = activeNGs(ngdb) flatMap (ng => ngdb(ng))
  // get nogoods that are active in this view
  private def activeNGs(ngdb: NGDB):Set[NoGood] = (ngdb getNGs) filter (_ isPartOf this)
  // is given no good unifiable with current view
  def matches(ng:NoGood):Boolean = ng isPartOf this
  // what are the nogoods2
  def computePureNoGoods:Set[NoGood] = ngc(id-1,Set(),Set())
  // what are the nogoods given nogoods implications
  def computeNewNoGoods(ngdb:NGDB):Set[NoGood] = {
    val actNGs = activeNGs(ngdb)
    val blocked = blockedByNoGoods(ngdb)
    val support = ((blocked flatMap (nval => ngdb(nval))) intersect actNGs) flatMap (ng => ng.keys)
    ngc(id-1,support,blocked)
  }
  //no goods computer
  private def ngc(agID:Int, support:Set[Int], blocked:Set[NVal]):Set[NoGood] = {
    if(curDomain.subsetOf(blocked)) {
      val m = support.map(key => key -> view(key).toNVal).toMap
      Set(NoGood(m))
    }else if(agID == -1) {
      Set()
    }else if(agID == id || !view.contains(agID)){
      ngc(agID-1,support,blocked)
    }else{
      val bs = blocks(agID)(view(agID).toNVal) intersect curDomain
      if(bs.forall(blocked.contains)){
        ngc(agID-1,support,blocked)
      }else{
        ngc(agID-1,support+agID,blocked++bs) ++ ngc(agID-1,support,blocked)
      }
    }
  }
  // transform this view to an Array
  def toArray:Array[Int] = {
    val arr = new Array[Int](n)
    (view keys) foreach (key => arr(key) = view(key).toInt)
    arr(id) = curPositon.toInt
    arr
  }
  // transform to List, used for solution verification
  def toList(t:Time):List[NValWithTime] = (0 until n) map ( i => if(i==id) NValWithTime(n,curPositon,t) else view(i)) toList
  // verification of solution
  def checkList(l:List[NValWithTime],ngdb:NGDB):Boolean = {
    def correspondsToView(tuple:(NValWithTime,Int)):Boolean = {
      val (nvl,i) = tuple
      if (i==id) curPositon == nvl.toNVal else containsKey(i) && view(i) == nvl
    }
    ((l zipWithIndex) forall correspondsToView) &&
    isConsistent(ngdb)
  }
  // classic to string
  override def toString:String ={
    import concurency.Listener._
    val sb = new StringBuilder()
    for(i <- 0 until n){
      if(i == id){
        sb.append(line(n,curPositon.toInt).mkString(Separator,Separator,Separator))
        sb.append("\n")
      }else if(view.contains(i)){
        sb.append(line(n,view(i).toInt).mkString(Separator,Separator,Separator))
        sb.append("\n")
      }else {
        sb.append(voidLine(n) mkString(Separator,Separator,Separator))
        sb.append("\n")
      }
    }
    sb.toString()
  }
}
case object View{
  // methods for computation of blocked positions
  def getBlocked4Args(agentID:Int)(y:Int)(x:Int)(n:Int):Set[NVal]=
    Set(NVal(n,x+(y-agentID)),NVal(n,x),NVal(n,x+(agentID-y))) filter (_.isValid)
  def getBlocked(agentID:Int)(whoBlocks:Int)(blocksFrom:NVal):Set[NVal] =
    getBlocked4Args(agentID)(whoBlocks)(blocksFrom.value)(blocksFrom.n)
}