package variables
import scala.collection.mutable.{Map => MMap}

case class NGDB(id:Int) {
  private val ngdb2val:MMap[NoGood,Set[NVal]] = MMap()
  private val val2ngdb:MMap[NVal,Set[NoGood]] = MMap()

  def add(ng:NoGood):Unit={
    val (nval,ngd) = ng.split(id)
    updateNgdb2val(ngd,nval)
    updateVal2ngdb(nval,ngd)
  }
  def getNGs:Set[NoGood] = ngdb2val.keys.toSet
  def apply(ng:NoGood):Set[NVal] = ngdb2val.getOrElse(ng,Set())
  def apply(nv:NVal):Set[NoGood] = val2ngdb(nv)

  private def updateNgdb2val(key:NoGood, value:NVal) = {
    if ( (!val2ngdb.contains(value)) || !(val2ngdb(value) exists (oldKey => oldKey isPartOf key))) {
      val oldKeys = ngdb2val.keys filter (oldKey => key isPartOf oldKey)
      oldKeys foreach (ok => {val set = ngdb2val(ok); ngdb2val put(ok, set - value)})
      val newlyEmpty = oldKeys filter (ngdb2val(_).isEmpty)
      newlyEmpty foreach ngdb2val.remove
      val set = ngdb2val.getOrElse(key, Set())
      ngdb2val put(key, set + value)
    }
  }
  private def updateVal2ngdb(key:NVal,value:NoGood) = {
    val oldNgds = val2ngdb.getOrElse(key,Set())
    if(!(oldNgds exists (_ isPartOf value))) {
      val updatedNgds = (oldNgds filterNot value.isPartOf) + value
      val2ngdb put(key, updatedNgds)
    }
  }

  override def toString: String = {
    if(val2ngdb.isEmpty) "EMPTY" else
    ngdb2val.toString()//.mkString("\n")
  }

}
