package variables

/**
  * this class represents a nogood
  * @param m it is based on a map: rowId -> forbidden value
  */
case class NoGood(m:Map[Int,NVal]) {
  def keys = m.keys                                                         // which rows does this NoGood take into acount
  def containsKey(key:Int):Boolean = m.contains(key)                        // is given row present?
  def apply(key:Int):NVal= m(key)                                           // get forbidden value for given row
  def isPartOf(view: View):Boolean = {                                      // is this NoGood part of given view?
    if(m.isEmpty) true else
    m.forall(x => view.containsKey(x._1) && x._2.value == view(x._1).value )
  }
  def isPartOf(ng:NoGood):Boolean = {                                        // is this NoGood part the other NoGood?
    if(m.isEmpty) true else
    m.forall(x => ng.containsKey(x._1) && x._2 == ng(x._1))
  }
  def remove(key:Int):NoGood = NoGood(m.filterKeys(_ != key))                 // create a new NoGood without given row
  def split(key:Int):(NVal,NoGood) = (m(key),remove(key))                    // split the NoGood on a value and a new NoGood
  def getJ:Int = keys.max                                                   // get id of an agent we should send this NoGood to

  override def toString: String = {
    def mToString = {
      val sb = new StringBuilder()
      m foreach (pair => sb.append(s"x_${pair._1}=${pair._2.toInt}, "))
      sb.toString()
    }
    s"NoGood($mToString)"
  }
}
case object NoGood{
  val EmptyNGood = NoGood(Map())                                             // a contradiction ~ empty NoGood
}