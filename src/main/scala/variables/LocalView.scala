package variables

/**
  * this class represents a local view of one agent.
  * it contains a [[variables.View]] where the view is stored and
  * a database of known [[variables.NoGood]] as an instance of [[variables.NGDB]]
 *
  * @param n        number of rows and number of colums and also number of agents
  * @param id       id of this particular agent
  */
case class LocalView(n:Int,id:Int) {
  val view:View = View(n,id)                    // structure containing local view and current position
  val ngdb:NGDB = NGDB(id)                      // NoGood database
  val confirmed = new Array[Boolean](n)          // is given position verified as feasible
  revokeAll

  // specific methods --------------------------------------------------------------------------------------------------
  def at(t:Time):NValWithTime = NValWithTime(n,curPositon,t)        // current position with time
  def revokeAll = (confirmed indices) foreach revoke                // revoke confirmation for all agents
  def revoke(key:Int) = confirmed(key) = false                      // revoke confirmation for position of given agent
  def confirm(key:Int) = confirmed(key) = true                      // confirm that the position is ok with agent at key
  lazy val indices = ((0 until n ) toSet ) - id                     // indices of others
  def isComplete:Boolean = indices forall containsKey               // the local view contains info about all other agents
  def notComplete:Boolean = !isComplete
  def missingKeys = indices filterNot containsKey                   // the agents whose position we don't know
  def isValidSolution = {
    isComplete && isConsistent &&
      (indices forall confirmed)                                    // solution is valid if all positions are verified
  }

  // provide methods of ngdb -------------------------------------------------------------------------------------------
  def add(ng: NoGood):Unit={                                        // add new NoGood to DB
    ngdb.add(ng)                                                    // add the NoGood
    view.recomputeDomain(ngdb)                                      // update domain, if needed
  }
  // get all NoGoods
  def getNGs:Set[NoGood] = ngdb.getNGs                               // return all nogoods present in DB

  // provide methods of view -------------------------------------------------------------------------------------------
  def curPositon:NVal = view.curPositon                             // current position
  def curDomain:Set[NVal] = view.curDomain                          // current domain
  def apply(key: Int):NValWithTime = view(key)                      // accessor for views
  def containsKey(key:Int):Boolean = view containsKey key           // does a value for given key exists?
  def put(key:Int,value:NValWithTime):Unit = view put(key,value)    // update positions of others
  def remove(key:Int):Unit = {revoke(key); view remove key }        // remove knowledge about other position of neighbour
  def recomputeDomain():Unit = view.recomputeDomain(ngdb)           // recompute current domain
  def isInconsistent:Boolean = view.isInconsistent(ngdb)            // is x_i inconsistent with the view?
  def isConsistent:Boolean = view.isConsistent(ngdb)                // is x_i consistent with the view?
  def existsConsistent:Boolean = view.existsConsistent(ngdb)        // is there some consistent value?
  def canMove:Boolean = view.canMove(ngdb)                          // can this agent move?
  def move2(newPosition:NVal):Unit = view.move2(newPosition)        // move this agent to position
  def getNonBlocked:Set[NVal] = view.getNonBlocked(ngdb)            // non blocked positions
  def allBlocked:Set[NVal] = view.allBlocked(ngdb)                  // get all blocked positions
  def matches(ng: NoGood):Boolean = view.matches(ng)                  // is given no good unifiable with current view
  def computePureNoGoods:Set[NoGood] = view.computePureNoGoods       // what are the nogoods
  def computeNewNoGoods:Set[NoGood] = view.computeNewNoGoods(ngdb)   // what are the nogoods given nogoods implications
  def toArray:Array[Int] = view toArray
  def toList(t:Time):List[NValWithTime] = view toList(t)
  def checkList(l:List[NValWithTime]):Boolean = view checkList(l,ngdb)  // check that the solution in the list is consistent with the view
  override def toString:String = {
    "----------------------------------------\n" +
    id+"\n" +view.toString + ngdb.toString
  }
}
