package concurency

import akka.actor.ActorRef
import messages._
import variables._
import Time._
import NoGood.EmptyNGood

case class QueenAgentActor(
                     agentId:Int,           // id of this agent, corresponds to the row it works with
                     nAgents:Int,           // number of all agents / # rows of the board
                     listener: ActorRef     // listener, used to channel the output to the outer space
                     ) extends QueenAgentTrait {
  val localView:LocalView = LocalView(nAgents,agentId)  // structure containing local view and current position
  var t:Time = Beginning                        // we start at time 0
  var neighbours: Array[ActorRef] = _           // other agents references

  override def receive: Receive = {
    case Init(nghbrs) =>                        // note: we just received the references to neighbours
      //println(s"$agentId:Init")
      neighbours = nghbrs                       // so we store them
    case StartMsg =>                            // note: here the algorithm starts. so we:
      //println(s"$agentId:Start")
      broadcast(OkMsg(agentId,localView.at(t))) // ask others if my position causes trouble
    case OkMsg(from,at) =>                      // note: someone is asking, if we are OK with his positon. so we:
      //println(s"$agentId:Ok")
                                                // if this information is new, we process it
      if(!localView.containsKey(from) || localView(from)!=at) {
        localView put(from, at)                 // update local view
        localView revoke from                   // revoke confirmations from given agent
        checkLocalView                          // check if the update caused some problem
      }
    case AddFriendMsg(hisID,ref) =>             // note: someone told us to add a new neighbour
      //println(s"$agentId:AddFriend")
      neighbours(hisID) = ref                   // WARNING: not used, as all the agents already are neighbours
    case NoGoodMsg(from,ng) =>                  // note: someone told us what is NoGood
      //println(s"$agentId:NoGood")
      localView add ng                          // add NoGood to the database
      // here we should add the new neighbours
      // and add the knowledge about their positions
      val oldPosition = localView.curPositon    // store the old position
      checkLocalView                            // check that the local view is consistent even with the new NoGood
      if(oldPosition != localView.curPositon)   // if we had to move, then we tell others where we are
        //broadcast(OkMsg(agentId,localView.at(t)))
        neighbours(from) ! OkMsg(agentId,localView.at(t))
    case Revoke(agent) =>                       // note: someone has doubt that position of agent is ok
      //println(s"$agentId:Revoke")
      localView revokeAll                       // set the position of agent as not confirmed
    case Verify(from,list,hisPositon) =>        // note: somebody says that he's ok with the solution in the list
      //println(s"$agentId:Verify")
      verify(from,list,hisPositon)              // process the verification
    case TerminateFailMsg =>                    // note: someone detected infeasibility of this instance
      //println(s"$agentId:Fail")
      Unit                                      // do nothing
    case ICQ(from:Int) =>                       // note: someone seeks this agent
      //println(s"$agentId:ICQ")
      neighbours(from) ! OkMsg(agentId,localView.at(t)) // so we state its position
    case IsCorrect(from,list) =>                // note: somebody asks us to check a solution
      //println(s"$agentId:ISCorrect")
      if (doublecheck(list))                    // we check that this solution is in accordance with our view, and causes no trouble
        neighbours(from) ! Verify(agentId, list, localView.at(t)) // send verification
      else                                      // if we have some objections, or our view is incomplete
        fillTheGaps                             // we ask other to tell us their positions
    case success:TerminateSuccessMsg =>         // note: someone succeeded in obtaining feasible solution
      listener ! success(agentId)               // return the successful solution
  }

  def broadcast[A <: Msgs](message:A):Unit = neighbours foreach ( _ ! message)  // send given message to all

  lazy val success:TerminateSuccessMsg = {      // successful end message
    val solution = localView.toArray            // transform current positions into an array
    TerminateSuccessMsg(agentId,solution)       // create termination message
  }

  def doublecheck(l:List[NValWithTime]):Boolean = localView checkList l  // check validity from our perspective
  def fillTheGaps:Unit = (localView missingKeys) foreach (neighbours(_) ! ICQ(agentId)) // ask others to send us positions


  def verify(from: Int, l: List[NValWithTime], hisPositon: NValWithTime): Unit = {
    if(doublecheck(l) &&         // if still are on the same position
      localView.containsKey(from) &&
      localView(from).toNVal == hisPositon.toNVal     // and his position is consistent with our view then
    ){
      localView confirm from                    // our position is consistent with hisPosition
      if(localView.isValidSolution) {           // if this solution is valid solution
        if ( agentId == nAgents-1) {            // if this agent has the highest priority, then success
          broadcast(success)                    // tell others we have succeeded
          listener ! success                    // return solution
        }
      }
    }else{
      broadcast(OkMsg(agentId,localView.at(t)))
    }
  }

  def checkLocalView: Unit = {
    if(localView.isInconsistent){               // if current value is not consistent, then
      if(localView.existsConsistent){           // if there is some consistent value in current domain
        val newPosition = localView.getNonBlocked.head  // a consistent position we move to
        localView.move2(newPosition)            // move agent to the selected position
        localView.revokeAll
        t = t.nextTime                          // update number of movements executed
        broadcast(OkMsg(agentId,localView.at(t))) // tell others about the move
      }else{
        backtrack
      }
    }else if(localView.notComplete){
      fillTheGaps
    }else if(agentId == nAgents-1){
      broadcast(IsCorrect(agentId,localView.toList(t)))
    }
  }

  def backtrack: Unit = {
    val nogoods = localView.computeNewNoGoods   // compute new NoGoods
    if(nogoods.contains(EmptyNGood)){           // if an empty NoGood is present, then there is no possible movement ever
      listener ! TerminateFailMsg()             // return the fail
    }else{
      nogoods.foreach(v => {
        localView remove v.getJ                 // remove the knowledge about position of j from local view
        neighbours(v.getJ) ! NoGoodMsg(agentId,v) // tell neighbour to handle NoGood
      })
      checkLocalView                            // we verify consistency of our local view
    }
  }

}
