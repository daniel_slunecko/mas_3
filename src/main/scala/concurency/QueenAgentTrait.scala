package concurency

import akka.actor.Actor

trait QueenAgentTrait extends Actor{
  val nAgents:Int
  val agentId:Int
}
