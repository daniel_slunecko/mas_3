package concurency

import akka.actor.{Actor}
import messages._

/**
  * this class is used by the main object to comunicate with all the other actors
  * @param n number of the actors we expect to receive data from
  */
case class Listener(n:Int) extends Actor{
  import Listener._
  var finnished:Set[Int] = Set()

  override def receive = {
    case TerminateSuccessMsg(from,solution) =>
      if(!finnished.contains(from)) {
        println(s">>> Agent at row $from has claimed success")
        finnished = finnished + from
      }
      if(finnished.size == n) {
        finnished = finnished + n
        printoutSolution(solution)
        context.system.terminate()
      }
    case TerminateFailMsg() =>
      if(finnished.isEmpty) {
        Console.err.println(s">>> One of the agents reported that no solution exists.")
        context.system.terminate()
        finnished = finnished + n
      }
  }

  private def printoutSolution(solution: Array[Int]):Unit = {
    println(">>> All agents have claimed to finnish successfully and the solution is:")
    (solution indices) foreach (i => printLine(solution(i)))
  }

  private def printLine(x:Int):Unit = {
    println (line(n,x) mkString (Separator,Separator,Separator))
  }
}
case object Listener{
  def voidLine(n:Int): Array[String] = (1 to n) map (_ => " ") toArray
  def line(n:Int,x:Int): Array[String] = {val l = voidLine(n); l(x) = Queen; l}
  lazy val Queen = "Q"
  lazy val Separator = "|"
}
