import StartAgents.Nagents
import akka.actor.{ActorRef, ActorSystem, Props}
import concurency.{Listener, QueenAgentActor}
import messages._

import scala.annotation.tailrec

object StartAgents extends App{
  // Change the number of agents/queens here or pass it as an argument
  val Nagents = 8

  def parseInput:Int = if (args.size < 1) Nagents else args.head.toInt
  val n = parseInput                                        // number of agents & size of the board

    val system = ActorSystem("Queens")                      // we create an actor system
    val listener =  system.actorOf(Props(Listener(n)),"Listener") // we create listener for message receiving
    val actors:Array[ActorRef] = createActors(n-1,List()).toArray // we create the actors representing queens
    actors foreach (actor => actor ! Init(actors))          // tell each actor who his neighbours are
    actors foreach (actor => actor ! StartMsg)              // start the show

    @tailrec
    def createActors(id:Int,actors:List[ActorRef]):List[ActorRef] = {
      if (id < 0) actors else {
        val queen = system.actorOf(Props(QueenAgentActor(id, n, listener)), name = s"$id")
        createActors(id - 1, queen :: actors)
      }
    }

}
