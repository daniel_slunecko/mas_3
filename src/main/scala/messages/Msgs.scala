package messages

import akka.actor.ActorRef
import variables.{NoGood, NValWithTime}

sealed trait Msgs
case class Init(neighbours:Array[ActorRef]) extends Msgs                          // initialize the actors
case class StartMsg() extends Msgs                                                // start the algorithm
case class OkMsg(from:Int, at:NValWithTime) extends Msgs                          // Ok message
case class AddFriendMsg(whom:Int,ref:ActorRef) extends Msgs                       // message to add friends. unused
case class NoGoodMsg(from:Int,ng:NoGood) extends Msgs                              // message to pass nogoods to other agents
case class TerminateFailMsg() extends Msgs                                        // Unsuccessful ending
@deprecated
case class Revoke(key:Int) extends Msgs                                           // revoke confirmation of some solution
case class IsCorrect(from:Int,solution:List[NValWithTime]) extends Msgs           // request for validation of solution
case class ICQ(from:Int) extends Msgs                                             // I seek the possition of given queen
case class Verify(from:Int,ok:List[NValWithTime],position:NValWithTime) extends Msgs    // we verify that this position is ok from our perspective
case class TerminateSuccessMsg(from:Int,solution:Array[Int]) extends Msgs{        // successful ending
  def apply(newFrom:Int) = TerminateSuccessMsg(newFrom,solution)
}
